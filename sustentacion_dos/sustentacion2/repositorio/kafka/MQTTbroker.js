//MQTT broker as a module

var mosca = require('mosca');

var mati = {
    //using ascoltatore
    type: 'mongo',
    url: 'mongodb://127.0.0.1:27018/mqtt',//'mongodb://0.0.0.0:27017/mqtt',,//'mongodb://localhost:27017/mqtt',
    pubsubCollection: 'mati',
    mongo: {}
};

var settings = {
    port: 1883,
    backend: mati
};

var server = new mosca.Server(settings);

server.on('clientConnected', function(client) {
    console.log('client connected', client.id);
});

// fired when a message is received
server.on('published', function(packet, client) {
    console.log('Published', packet.payload);
    console.log(packet.payload)
});

server.on('ready', setup);

// fired when the mqtt server is ready
function setup() {
    console.log('Mosca server is up and running');
}


server.on('published', function(packet, client) {
    console.log('Published', packet);
    console.log('Client', client);
});
// fired when a client connects
server.on('clientConnected', function(client) {
    console.log('Client Connected:', client.id);
});

// fired when a client disconnects
server.on('clientDisconnected', function(client) {
    console.log('Client Disconnected:', client.id);
});