/**
 * Created by OscarVargas on 2/27/16.
 */


Mapping =[

    {mqtt:"celsius",kafka:"celsius"},
    {mqtt:"fahrenheit",kafka:"fahrenheit"},
    {mqtt:"temperature",kafka:"temperature"}

];

// 1. mqtt: kafka mapping
 MappingEnum = {
    celsius : "celsius",
    fahrenheit : "fahrenheit",
    temperature : "temperature",
    test: "temperature"
};