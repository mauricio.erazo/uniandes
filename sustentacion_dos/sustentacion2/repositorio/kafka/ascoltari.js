/**
 * Created by OscarVargas on 2/27/16.
 */
var ascoltatori = require('ascoltatori');
var MongoClient = require('mongodb').MongoClient;



MongoClient.connect('mongodb://localhost:27017/mqtt', {}, function (err, db) {
    var settings = {
        type: 'mongo',
        db: db,
        pubsubCollection: 'mati'
    };

    var databasemqtt =db;

    ascoltatori.build(settings, function (err, ascoltatore) {

        ascoltatore.subscribe('temperature/celsius', function() {
            console.log(arguments[2].messageId +":"+arguments[1].toString());

            var collection = db.collection('log');

            collection.insert({id:arguments[2].messageId,message:arguments[1].toString(),topic:arguments[0].toString()}, function(err, result){
                console.info (err || result);
            });
        });


        ascoltatore.subscribe('temperature/fahrenheit', function() {
            console.log(arguments[2].messageId +":"+arguments[1].toString());

            var collection = db.collection('log');

            collection.insert({id:arguments[2].messageId,message:arguments[1].toString(),topic:arguments[0].toString()}, function(err, result){
                console.info (err || result);
            });
        });



    });
})

/*
var ascoltatori = require('ascoltatori');
settings = {
    type: 'mqtt',
    json: false,
    mqtt: require('mqtt'),
    url: 'mqtt://127.0.0.1:1883'
};

ascoltatori.build(settings, function (err, ascoltatore) {
    ascoltatore.subscribe('temperature/celsius', function() {
        console.log(arguments);

      var topic=  arguments[0].toString();
        var value = arguments[1].toString();
        console.log(arguments[1].toString())
        // { '0': 'hello', '1': 'a message' }
    });

    // publishes a message to the topic 'hello'
    ascoltatore.publish('illumination', 'a message', function() {
        console.log('message published');
    });
});*/