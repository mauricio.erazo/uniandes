package com.mapr.examples;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 * This producer will send a bunch of messages to topic "fast-messages". Every so often,
 * it will send a message to "slow-messages". This shows how messages can be sent to
 * multiple topics. On the receiving end, we will see both kinds of messages but will
 * also see how the two topics aren't really synchronized.
 */
public class Producer {
    public static void main(String[] args) throws IOException {
        // set up the producer
        KafkaProducer<String, String> producer;
        try (FileInputStream in = new FileInputStream(new File("./resources/producer.props"))) {
        	
            Properties properties = new Properties();
            properties.load(in);
            producer = new KafkaProducer<>(properties);
        }

        String []latitudes = { "40.75793", "43.9939","39.75693", "53.77890"};
        String []longitudes= { "-4.793", "3.9889","9.7474", "45.99393"};
        int indiceLat = (int)Math.floor(Math.random()*(4)+1) - 1;
        int indiceLon = (int)Math.floor(Math.random()*(4)+1) - 1;
        
        try {
            for (int i = 0; i < 1; i++) {
                // send lots of messages
            	System.out.println(String.format("{\"type\":\"beacon signal\", \"latitud\":%s, \"longitud\":%s, \"thread name\":%s}", latitudes[indiceLat], longitudes[indiceLon], Thread.currentThread().getName()));
                producer.send(new ProducerRecord<String, String>(
                        "beacon",
                        String.format("{\"type\":\"beacon signal\", \"latitud\":%s, \"longitud\":%s, \"thread name\":%s}", latitudes[indiceLat], longitudes[indiceLon], Thread.currentThread().getName())));
                producer.flush();
            }
        } catch (Throwable throwable) {
            System.out.printf("%s", throwable.getStackTrace());
        } finally {
            producer.close();
        }

    }
}
